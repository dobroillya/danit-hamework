import NavBar from '../components/NavBar';
import ListProducts from '../components/ListProducts/ListProducts';
import Modal from '../components/Modal/Modal';
import Button from '../components/Button/Button';
import { RootState, removeFromBasket } from '../store';
import { useDispatch, useSelector } from 'react-redux';
import { listBasket } from '../utils/helper';
import FormBasket from '../components/Form/Form';
import { ProductDisplayProvider } from '../context/ProductDisplayContext';

function Basket() {
  const dispatch = useDispatch();

  const { data: products } = useSelector((state: RootState) => {
    return state.products;
  });

  return (
    <div className="wrapper">
      <NavBar />
      <Modal
        heading="Are you sure to delete product?"
        text=""
        closeButton
        actions={
          <Button
            backgroundColor="#3e4c6b"
            onClick={() => dispatch(removeFromBasket())}
          >
            Ok
          </Button>
        }
      />
      {listBasket(products).length > 0 ? (
        <>
          <ProductDisplayProvider>
            <ListProducts type="basket" />
          </ProductDisplayProvider>
          <FormBasket />
        </>
      ) : (
        'No products in basket'
      )}
    </div>
  );
}

export default Basket;
