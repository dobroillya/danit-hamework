import NavBar from '../components/NavBar';
import ListProducts from '../components/ListProducts';
import { useSelector } from 'react-redux';
import { RootState } from '../store';
import { listFavorit } from '../utils/helper';
import { ProductDisplayProvider } from '../context/ProductDisplayContext';

function Favorit() {
  const { data: products } =  useSelector((state:  RootState) => {
    return state.products;
  });


  return (
    <div className="wrapper">
      <NavBar />
      {listFavorit(products).length > 0 ?  <ProductDisplayProvider> <ListProducts type='favorit' /></ProductDisplayProvider> : "No products in favorit"}
    </div>
  );
}

export default Favorit;
