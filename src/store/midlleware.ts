import { createListenerMiddleware, isAnyOf } from "@reduxjs/toolkit";
import {
  addToFavorit,
  removeFromFavorit,
  handleOkButton,
  removeFromBasket
} from './slices/productsReduser.ts';
import type { RootState } from "./index";

export const listenerMiddleware = createListenerMiddleware();
listenerMiddleware.startListening({
  matcher: isAnyOf(addToFavorit,
    removeFromFavorit,
    handleOkButton,
    removeFromBasket
    ),
  effect: (_,listenerApi) =>  {

    localStorage.setItem(
    "cartItems",
    JSON.stringify((listenerApi.getState() as RootState).products.data)
    )
}

});
