import { createSlice } from '@reduxjs/toolkit';
import { fetchProducts } from '../thunk/fetchProducts';
import { ProductType } from '../../App';

export interface ProductsState {
  data: ProductType[];
  isLoading?: boolean;
  error?: null;
  isModalOpen?: boolean;
  currentProduct?: string;
  firstLoading?: boolean;
}

export const initialState = {
  data: [],
  isLoading: false,
  error: null,
  isModalOpen: false,
  currentProduct: '',
  firstLoading: true,
} as ProductsState;


// const persistedState = localStorage.getItem('cartItems')
//                        ? JSON.parse(localStorage.getItem('cartItems') || '{}')
//                        : {}



const productsSlice = createSlice({
  name: 'products',
  initialState,
  reducers: {
    addToFavorit(state, action) {
      state.data = state.data.reduce<any>((acc, el: ProductType) => {
        el.sku === action.payload.sku
          ? acc.push({ ...el, countFavorit: !el.countFavorit })
          : acc.push({ ...el });
        return acc;
      }, [])
    },
    removeFromFavorit(state, action) {
      console.log(action.payload);
      state.data = state.data.reduce<any>((acc, el: ProductType) => {
        el.sku === action.payload
          ? acc.push({ ...el, countFavorit: false })
          : acc.push({ ...el });
        return acc;
      }, []);
    },
    addToBasket(state, action) {
      state.currentProduct = action.payload.sku;
      state.isModalOpen = true;
    },
    handleOkButton(state) {
      state.isModalOpen = false;
      state.data = state.data.reduce<any>((acc, el: ProductType) => {
        el.sku === state.currentProduct
          ? acc.push({ ...el, countBasket: ++el.countBasket})
          : acc.push({ ...el });
        return acc;
      }, []);
    },
    handleCloseBtn(state) {
      state.isModalOpen = false;
    },

    removeFromBasket(state) {
      // state.searchTerm = action.payload;
      state.data = state.data.reduce<any>((acc, el: ProductType) => {
        el.sku === state.currentProduct
          ? acc.push({ ...el, countBasket: 0 })
          : acc.push({ ...el });
        return acc;
      }, []);
      state.isModalOpen = false;
    },
    reset(state) {
      state.data = state.data.reduce<any>((acc, el: ProductType) => {
        acc.push({ ...el, countFavorit: false, countBasket: 0 })

        return acc;
      }, []);
    }
  },
  extraReducers(builder) {
    builder.addCase(fetchProducts.pending, (state) => {
      state.isLoading = true;
    });
    builder.addCase(fetchProducts.fulfilled, (state, action) => {
      state.isLoading = false;
      if (state.firstLoading) {
        state.data = action.payload;
        state.firstLoading = false;
      }
    });
    builder.addCase(fetchProducts.rejected, (state) => {
      state.isLoading = false;
      // state.error = action.error;
    });
  },
});

export const productsReducer = productsSlice.reducer;
export const {
  addToFavorit,
  removeFromFavorit,
  addToBasket,
  removeFromBasket,
  handleOkButton,
  handleCloseBtn,
  reset
} = productsSlice.actions;
