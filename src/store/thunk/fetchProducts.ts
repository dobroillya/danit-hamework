import { createAsyncThunk } from '@reduxjs/toolkit';
import axios from 'axios';

const fetchProducts = createAsyncThunk('users/fetch', async () => {
  const response = await axios.get('../data.json');

  return response.data;
});


export { fetchProducts };
