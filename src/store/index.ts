import { configureStore } from '@reduxjs/toolkit';
import { productsReducer, ProductsState } from './slices/productsReduser.ts';
import {
  addToFavorit,
  removeFromFavorit,
  addToBasket,
  removeFromBasket,
  handleOkButton,
  handleCloseBtn,
  reset
} from './slices/productsReduser.ts';
import { listenerMiddleware } from './midlleware.ts';


const prodState =  JSON.parse(localStorage.getItem("cartItems") || "null");

const initState: ProductsState = {
  data: prodState || [] ,
  isLoading: false,
  error: null,
  isModalOpen: false,
  currentProduct: '',
  firstLoading: true
}

export const store = configureStore({
  preloadedState: {
    products: initState
  },
  reducer: {
    products: productsReducer
  },
  middleware: (getDefaultMiddleware) => [
    ...getDefaultMiddleware(),
    listenerMiddleware.middleware
  ]
});

// Infer the `RootState` and `AppDispatch` types from the store itself
export type RootState = ReturnType<typeof store.getState>;
// Inferred type:  {    products: productsReducer,    favorit: favoritReducer,   basket: basketReducer,  }
export type AppDispatch = typeof store.dispatch;

export * from './thunk/fetchProducts.ts';
export { addToFavorit, removeFromFavorit, addToBasket, removeFromBasket, handleOkButton, handleCloseBtn, reset };
