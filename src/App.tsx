import { useEffect } from 'react';
import Button from './components/Button';
import Modal from './components/Modal/Modal';
import ListProducts from './components/ListProducts/';
import NavBar from './components/NavBar/NavBar';
import { useThunk } from '../src/hooks/use-thunk';
import { fetchProducts } from '../src/store/thunk/fetchProducts';
import {  useDispatch } from 'react-redux';
import { handleOkButton } from './store';
import { ProductDisplayProvider } from './context/ProductDisplayContext';

export type ProductType = {
  name?: string;
  price?: number;
  image?: string;
  sku: string;
  color?: string;
  countFavorit: boolean;
  countBasket: number;
};


function App() {

  const dispatch = useDispatch();

  const [doFetchProducts] = useThunk(fetchProducts);


  useEffect(() => {
    console.log(!localStorage.getItem("cartItems"));
    if(!localStorage.getItem("cartItems")){
      doFetchProducts()
    }
  }, [doFetchProducts]);


  return (
    <>
      <div className="wrapper">
        <NavBar/>
          <Modal
            heading='Are you sure?'
            text=''
            closeButton
            actions={
              <Button backgroundColor='#3e4c6b' onClick={() => dispatch(handleOkButton())}>
                Ok
              </Button>
            }
          />
      <ProductDisplayProvider>
        <ListProducts
          type='all'
        />
        </ProductDisplayProvider>
      </div>
    </>
  );
}

export default App;
