import React from 'react';
import ReactDOM from 'react-dom/client';
import App from './App.tsx';
// import { Provider } from './context/ProductContext.tsx';
import { Provider } from 'react-redux';
import { store } from './store';
import './index.css';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import ErrorPage from './error-page';
import Basket from './pages/Basket-page.tsx';
import Favorit from './pages/Favorit-page.tsx';

const router = createBrowserRouter([
  {
    path: '/',
    element: <App />,
    errorElement: <ErrorPage />,
  },
  {
    path: 'favorit',
    element: <Favorit />,
  },
  {
    path: 'basket',
    element: <Basket />,
  },
]);

ReactDOM.createRoot(document.getElementById('root') as HTMLElement).render(
  <React.StrictMode>
    <Provider store={store}>
      <RouterProvider router={router} />
      <div className="modal-container"></div>
    </Provider>
  </React.StrictMode>
);
