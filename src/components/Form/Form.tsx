import { Formik, Form, Field } from 'formik';
import * as Yup from 'yup';
import { PatternFormat } from 'react-number-format';
import styled from 'styled-components';
import { useDispatch, useSelector } from 'react-redux';
import { RootState, reset } from '../../store';
import { listBasket } from '../../utils/helper';

interface MyFormValues {
  firstName: string;
  surname: string;
  age: number | string;
  address: string;
  phone: string;
}

const userSchema = Yup.object().shape({
  firstName: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  surname: Yup.string()
    .min(2, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  age: Yup.number()
    .required()
    .positive()
    .integer()
    .min(1)
    .max(120)
    .typeError('you must specify a number'),
  address: Yup.string()
    .min(5, 'Too Short!')
    .max(50, 'Too Long!')
    .required('Required'),
  phone: Yup.string()
    .required('Required')
    .matches(/^[0-9\-())\s]+$/, 'is not valid'),
});

const initialValues: MyFormValues = {
  firstName: '',
  surname: '',
  age: '',
  address: '',
  phone: '',
};

function FormBasket() {
  const { data: products } =  useSelector((state:  RootState) => {
    return state.products;
  });
  const dispatch = useDispatch();

  return (
    <div>
      <Formik
        initialValues={initialValues}
        validationSchema={userSchema}
        onSubmit={(values) => {

          console.log(values);
          console.log(listBasket(products));
          localStorage.removeItem('cartItems');
          dispatch(reset())


        }}
      >
        {({ errors, touched }) => (
          <StyledForm>
            <StyledLabel>
              Ім'я користувача:
              <StyledField
                type="text"
                id="firstName"
                name="firstName"
                placeholder="First Name"
              />
              {errors.firstName && touched.firstName ? (
                <StyledErrorMessage>{errors.firstName}</StyledErrorMessage>
              ) : null}
            </StyledLabel>
            <StyledLabel>
              Прізвище користувача:
              <StyledField
                type="text"
                id="surname"
                name="surname"
                placeholder="surname"
              />
              {errors.surname && touched.surname ? (
                <StyledErrorMessage>{errors.surname}</StyledErrorMessage>
              ) : null}
            </StyledLabel>
            <StyledLabel>
              Вік користувача:
              <StyledField type="text" id="age" name="age" placeholder="age" />
              {errors.age && touched.age ? (
                <StyledErrorMessage>{errors.age}</StyledErrorMessage>
              ) : null}
            </StyledLabel>
            <StyledLabel>
              Адреса доставки:
              <StyledField
                type="text"
                id="address"
                name="address"
                placeholder="address"
              />
              {errors.address && touched.address ? (
                <StyledErrorMessage>{errors.address}</StyledErrorMessage>
              ) : null}
            </StyledLabel>
            <StyledLabel>
              Мобільний телефон:
              <StyledField
                type="text"
                id="phone"
                name="phone"
                placeholder="phone"
              >
                {({ field }: any) => (
                  <PatternFormat format="(380) ###-##-##" mask="_" {...field} />
                )}
              </StyledField>
              {errors.phone && touched.phone ? (
                <StyledErrorMessage>{errors.phone}</StyledErrorMessage>
              ) : null}
            </StyledLabel>
            <StyledSubmitButton type="submit">Надіслати</StyledSubmitButton>
          </StyledForm>
        )}
      </Formik>
    </div>
  );
}

export default FormBasket;

const StyledForm = styled(Form)`
  display: flex;
  flex-direction: column;
  gap: 5px;
  max-width: 400px;
  align-items: start;
  box-shadow: 0px 8px 16px -6px rgba(0, 0, 0, 0.75);
  border-radius: 5px;
  padding: 8px;
  margin: 10px auto;
`;

const StyledLabel = styled.label`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  width: 100%;
`;

const StyledField = styled(Field)`
  /* Add your desired styles for the input fields here */
`;

const StyledErrorMessage = styled.div`
  color: red;
`;

const StyledSubmitButton = styled.button`
  align-self: center;
  margin-top: 8px;
  cursor: pointer;
`;
