import { useContext } from 'react'
import Product from '../Product';
import styled from 'styled-components';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';
import { listBasket, listFavorit } from '../../utils/helper';
import { ProductDisplayContext } from '../../context/ProductDisplayContext';


interface Props {
  type: string
}

function ListProducts({ type }: Props) {

  const { data: products } = useSelector((state:  RootState) => {
    return state.products;
  });
  const { isTableDisplay, toggleDisplay } = useContext(ProductDisplayContext);

  console.log(isTableDisplay);




  let content = products;
  switch (type) {
    case 'basket':
      content = listBasket(products);
      break;
    case 'favorit':
      content = listFavorit(products);
      break;
  }

  return (<Wrapper>
      <Label>
        Display as Table:
        <input type="checkbox" checked={isTableDisplay} onChange={toggleDisplay} />
      </Label>
    <List $primary={isTableDisplay}>
      <Label>
        Display as Table:
        <input type="checkbox" checked={isTableDisplay} onChange={toggleDisplay} />
      </Label>
      {content.map((el) => (
        <Product key={el.sku} data={el} type={type} isTableDisplay={isTableDisplay}/>
      ))}
    </List>
    </Wrapper>

  );
}

export default ListProducts;

const Wrapper = styled.div`
position: relative;
`

const Label = styled.label`
  position: absolute;
  top: -40px;
  left: 40px;
  font-size: 1rem;
  display: flex;
  gap: 4px;
  align-items: center;
`

const List = styled.ul<{ $primary?: boolean }>`

  display: flex;
  flex-direction: ${props => props.$primary ? 'column' : 'row'};
  margin-top: 48px;
  display: flex;
  flex-wrap: wrap;
  gap: 24px;
  padding-bottom: 48px;
`;
