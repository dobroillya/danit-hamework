import { MouseEventHandler } from 'react';
import styled from 'styled-components';

interface Props {
  children: string;
  backgroundColor: string;
  onClick: MouseEventHandler<HTMLButtonElement>;
}

interface ButtonProps {
  bgColor: string;
}

function Button({ backgroundColor, onClick, children }: Props) {
  return (
    <ButtonStyled bgColor={backgroundColor} onClick={onClick}>
      {children}
    </ButtonStyled>
  );
}

export default Button;

const ButtonStyled = styled.button<ButtonProps>`
  cursor: pointer;
  background-color: ${(props) => props.bgColor};
  color: white;
  border-radius: 3px;
  padding: 6px 24px;
  text-decoration: none;
  border: transparent;
`;
