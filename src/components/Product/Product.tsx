import { addToFavorit, addToBasket, removeFromFavorit } from '../../store';
import styled from 'styled-components';
import { IoIosPricetags } from 'react-icons/io';
import { BsFillBasket2Fill, BsFillBagHeartFill, BsXLg } from 'react-icons/bs';
import { ProductType } from '../../App';
import { useDispatch } from 'react-redux';

interface Props {
  data: ProductType;
  type: string;
  isTableDisplay: boolean;
}

function Product({ data, type, isTableDisplay }: Props) {
  const dispatch = useDispatch();

  return (
    <Item $primary={isTableDisplay}>
      {type === 'all' && (
        <>
          <Favorit $primary={isTableDisplay} onClick={() => dispatch(addToFavorit(data))}>
            <BsFillBagHeartFill
              color={data.countFavorit ? 'orange' : 'gray'}
              size="2rem"
            />
          </Favorit>
          <Basket onClick={() => dispatch(addToBasket(data))}>
            <BsFillBasket2Fill
              color={data.countBasket ? 'green' : 'gray'}
              size="2rem"
            />
          </Basket>
        </>
      )}

      {type === 'basket' && (
        <>
          <DeleteItem>
            <BsXLg size="1.2rem" onClick={() => dispatch(addToBasket(data))} />
          </DeleteItem>
        </>
      )}
      {type === 'favorit' && (
        <>
          <DeleteItem>
            <BsXLg
              size="1.2rem"
              onClick={() => dispatch(removeFromFavorit(data.sku))}
            />
          </DeleteItem>
        </>
      )}

      <ImageWrapper $primary={isTableDisplay}>
        <img src={data.image} alt="item" />
      </ImageWrapper>
      <Info $primary={isTableDisplay}>
        <Title $primary={isTableDisplay}>{data.name}</Title>
        <Price>
          <IoIosPricetags />
          {data.price}
        </Price>
      </Info>
    </Item>
  );
}

export default Product;

const Item = styled.div<{ $primary?: boolean }>`
  position: relative;
  display: flex;
  flex-direction: ${props => props.$primary ? 'row' : 'column'};
  gap: 4px;
  margin-left: auto;
  margin-right: auto;
  width: ${props => props.$primary ? '80%' : '300px'};
  box-shadow: 3px 3px 5px 0px rgba(0, 0, 0, 0.5);
`;

const ImageWrapper = styled.div<{ $primary?: boolean }>`
  img {
    width: ${props => props.$primary ? '100px' : '100%'};
    height: ${props => props.$primary ? '100px' : '300px'};
    object-fit: cover;
  }
`;

const Info = styled.div<{ $primary?: boolean }>`
  padding: 4px 12px;
  display: flex;
  gap:  ${props => props.$primary ? '24px' : '4px'};;
  align-items: center;
  justify-content: space-between;
`;

const Title = styled.h2<{ $primary?: boolean }>`
  font-size: ${props => props.$primary ? '2rem' : '1.1rem'};
  font-weight: 400;
`;

const Price = styled.p`
  display: flex;
  gap: 4px;
  align-items: center;
  font-weight: 400;
  padding: 2px 4px;
  color: white;
  border-radius: 20%;
  background-color: #563d2a;
`;

const Favorit = styled.div<{ $primary?: boolean }>`
  padding: 4px;
  position: absolute;
  right: ${props => props.$primary ? '80px' : '250px'};
  cursor: pointer;
  svg {
    transition: all 0.2s ease-in-out;
  }
  svg:hover {
    fill: #52875e;
  }
`;

const Basket = styled.div`
  position: absolute;
  right: 0;
  padding: 4px;
  cursor: pointer;
  svg {
    transition: all 0.2s ease-in-out;
  }
  svg:hover {
    fill: #e37b1a;
  }
`;

const DeleteItem = styled.div`
  position: absolute;
  right: 0;
  padding: 4px;
  cursor: pointer;
`;
