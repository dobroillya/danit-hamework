import styled from 'styled-components';
import { GrClose } from 'react-icons/gr';
import ReactDOM from 'react-dom';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';
import { handleCloseBtn } from '../../store';
import { useDispatch } from 'react-redux';

interface Props {
  heading: string;
  text: string;
  closeButton: boolean;
  actions: React.ReactNode;
}

function Modal({ heading, text, closeButton, actions }: Props) {
  // const { handleCloseBtn, isModalOpen } = useContext(ProductsContext);
  const dispatch = useDispatch();
  const { isModalOpen } = useSelector((state:  RootState) => {
    return state.products;
  });


  return ReactDOM.createPortal(
    <div>
      {isModalOpen && (
        <Wrapper onClick={() => dispatch(handleCloseBtn())}>
          <Content onClick={(e) => e.stopPropagation()}>
            <Header>
              <h2>{heading}</h2>
              {closeButton && (
                <CloseBtn onClick={() => dispatch(handleCloseBtn())}>
                  <GrClose />
                </CloseBtn>
              )}
            </Header>
            <Text>{text}</Text>
            <WrapperBtn>{actions}</WrapperBtn>
          </Content>
        </Wrapper>
      )}
    </div>,
    document.body
  );
}

export default Modal;

const Wrapper = styled.div`
  position: fixed;
  inset: 0;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  transition: all 0.3s ease-in-out;
  overflow: hidden;
  background-color: #8b8b8b6c;
  padding: 40px 20px 20px;
`;

const Content = styled.div`
  width: 300px;
  height: auto;
  background-color: #282c34;
  color: #fff;
`;

const Header = styled.header`
  position: relative;
  text-align: center;
  padding: 4px;
  background-color: #3e4c6b;
`;

const Text = styled.p`
  padding: 14px;
  text-align: center;
`;

const WrapperBtn = styled.div`
  padding-bottom: 24px;
  display: flex;
  gap: 12px;
  justify-content: center;
`;

const CloseBtn = styled.div`
  position: absolute;
  right: 8px;
  top: 8px;
  cursor: pointer;
  svg {
    pointer-events: none;
  }
`;
