import styled from 'styled-components';
import { BsFillBasket2Fill, BsFillBagHeartFill, BsHouse } from 'react-icons/bs';
import { Link } from 'react-router-dom';
import { calcBasket, calcFavorit } from '../../utils/helper';
import { useSelector } from 'react-redux';
import { RootState } from '../../store';

function NavBar() {

  const { data } = useSelector((state:  RootState) => {
    return state.products;
  });


  return (
    <Header>
      <Link to="/">
        <BsHouse size="2rem" />
      </Link>
      <Group>
        <Link to="/favorit" >
          <Favorit>
            <Count>{calcFavorit(data)}</Count>
            <BsFillBagHeartFill size="2rem" />
          </Favorit>
        </Link>
        <Link to="/basket">
          <Basket>
            <Count>{calcBasket(data)}</Count>
            <BsFillBasket2Fill size="2rem" />
          </Basket>
        </Link>
      </Group>
    </Header>
  );
}

export default NavBar;

const Header = styled.header`
  display: flex;
  padding: 16px;
  padding-left: 46px;
  background-color: #a88a5d23;
`;

const Favorit = styled.div`
  cursor: pointer;
  display: flex;
  position: relative;
`;

const Basket = styled.div`
  cursor: pointer;
  display: flex;
  position: relative;
`;

const Count = styled.div`
  width: 20px;
  height: 20px;
  left: -5px;
  top: -5px;
  background-color: #4d642b;
  color: white;
  position: absolute;
  padding: 4px;
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const Group = styled.div`
  margin-left: auto;
  display: flex;
  gap: 24px;
`;
