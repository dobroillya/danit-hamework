import React, { createContext, useState } from 'react';

type ProductDisplayContext = {
  isTableDisplay: boolean;
  toggleDisplay: ()=> void

}

type Props = {
  children: React.ReactNode
}

export const ProductDisplayContext = createContext({ } as ProductDisplayContext);

export const ProductDisplayProvider = ({ children }: Props) => {
  const [isTableDisplay, setIsTableDisplay] = useState(false);

  const toggleDisplay = () => {
    setIsTableDisplay((prevDisplay) => !prevDisplay);
  };

  return (
    <ProductDisplayContext.Provider value={{ isTableDisplay, toggleDisplay }}>
      {children}
    </ProductDisplayContext.Provider>
  );
};