import { ProductType } from "../App";

export function calcBasket(arr: any[]) {
  return arr.filter((el) => el.countBasket > 0).length;
}

export function calcFavorit(arr: any[]) {
  return arr.filter((el) => el.countFavorit).length;
}

export function getLocalStorage(){
  return JSON.parse(localStorage.getItem('cartItems')|| '{}')
}


export function listBasket(data: ProductType[]) {
  return data.filter((el: ProductType) => el.countBasket > 0);
}

export function listFavorit(data: ProductType[]) {
  return data.filter((el: ProductType) => el.countFavorit === true);
}