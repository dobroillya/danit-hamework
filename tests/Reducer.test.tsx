import {  addToFavorit, removeFromFavorit, addToBasket, handleCloseBtn } from '../src/store/index';
import { initialState, productsReducer } from '../src/store/slices/productsReduser';


describe('productsReducer', () => {
  it('should handle addToFavorit', () => {
    const initialState = {
      data: [
        { sku: "1", countFavorit: false, },
        { sku: "2", countFavorit: false }
      ],
      isLoading: false,
      error: null,
      isModalOpen: false,
      currentProduct: '',
      firstLoading: true,
    };
    const action = { type: addToFavorit.type, payload: { sku: "1" } };
    const state = productsReducer(initialState, action);
    expect(state.data[0].countFavorit).toBe(true);
    expect(state.data[1].countFavorit).toBe(false);
  });

  it('should handle removeFromFavorit', () => {
    const initialState = {
      data: [
        { sku: "1", countFavorit: true },
        { sku: "2", countFavorit: true }
      ]
    };
    const action = { type: removeFromFavorit.type, payload: "1" };
    const state = productsReducer(initialState, action);
    expect(state.data[0].countFavorit).toBe(false);
    expect(state.data[1].countFavorit).toBe(true);
  });

  it('should handle addToBasket', () => {
    const initialState = {
      isModalOpen: false,
      currentProduct: ''
    };
    const action = { type: addToBasket.type, payload: { sku: 'ABC123' } };
    const state = productsReducer(initialState, action);
    expect(state.isModalOpen).toBe(true);
    expect(state.currentProduct).toBe('ABC123');
  });

  it('should handle handleCloseBtn', () => {
    const initialState = {
      data: [
        { sku: "1", countFavorit: false, },
        { sku: "2", countFavorit: false }
      ],
      isModalOpen: true
    };
    const action = { type: handleCloseBtn.type };
    const state = productsReducer(initialState, action);
    expect(state.isModalOpen).toBe(false);
  });

  // Add more test cases for other actions

  it('should return the initial state if action type is not recognized', () => {
    const action = { type: 'UNKNOWN_ACTION' };
    const state = productsReducer(undefined, action);
    expect(state).toEqual(initialState);
  });
});