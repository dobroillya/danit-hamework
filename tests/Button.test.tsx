import { fireEvent, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import renderer from 'react-test-renderer';
import Button from '../src/components/Button';

const buttonProps = {
  backgroundColor: 'green',
  onClick: function () {
    console.log('submitted');
  },
};

test('snapshot button', () => {
  const tree = renderer
    .create(<Button {...buttonProps}>submit</Button>)
    .toJSON();
  expect(tree).toMatchSnapshot();
});

test('button test exist', () => {
  render(<Button {...buttonProps}>submit</Button>);

  const buttonElement = screen.getByRole('button');
  expect(buttonElement).toBeInTheDocument();
  expect(buttonElement).toHaveTextContent('submit');
});

test('button has background attribute', () => {
  render(<Button {...buttonProps}>submit</Button>);

  expect(screen.getByRole('button')).toHaveStyle(
    'background-color: rgb(0, 128, 0)'
  );
});

test('button has onclick event', () => {
  render(<Button {...buttonProps}>submit</Button>);

  fireEvent.click(screen.getByRole('button'));
});
