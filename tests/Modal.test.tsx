import { fireEvent, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';
import Modal from '../src/components/Modal';
import Button from '../src/components/Button';
import React from 'react';
import { Provider } from 'react-redux';
import { store } from '../src/store';
import { createStore } from '@reduxjs/toolkit';



const modalProps = {
  heading: "Modal title",
  text: "Modal text",
  actions: <Button backgroundColor='#3e4c6b' onClick={() => ('')}>
  Ok
</Button>,
  closeButton: true
};

it('is text in modal exist', () => {
  const store = createStore(() => ({
    products: {
      isModalOpen: true,
    },
  }));

  render(
    <Provider store={store}>
      <Modal heading="Modal Heading" text="Modal Text" closeButton={true} actions={<div>Modal Actions</div>} />
    </Provider>
  );

  const modalWindow = screen.getByText('Modal Text');
  expect(modalWindow).toBeInTheDocument();
})